import requests,csv,os,re
from bs4 import BeautifulSoup
# There is if main at the end of the script

'''
The function will try to find the contact page then to identify the contact form.
After that it will put in a CSV file all the link of contact page and a score for identified form.
The score will is an indicator of confiance.
(more there is string found in the form more there is probability it's a "contact form")

Function that get:
(entryCsvFile) a string, path of csv input
(csvFile) a string, the name of the result file ,
(stringToTests) array of string, list of string to check there is in the form to identify it
()
'''
def tenPoints(entryCsvFile,csvFile, stringToTests,phoneRequired,contactStringCheck):
    if os.path.exists(csvFile):
        os.remove(csvFile)
    fileResult = open(csvFile,"a")
    with open(entryCsvFile,newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        lineCount = 0
        for row in spamreader:
            lineCount = lineCount + 1
            #print(row[0])
            urlTorequest = row[0]
            htmlpage = requests.get(urlTorequest)
            soup = BeautifulSoup(htmlpage.text,'html.parser')
            # There is a problem here... what if there is a contact link but we didn't match the string...
            finalUrlContact = False
            urlContacts =  soup.find_all("a")
            print("URL WEBSITE:"+urlTorequest)
            for urlContact in urlContacts:
                if urlContact.string:
                    if re.search("contact us", urlContact.string,re.IGNORECASE):
                        finalUrlContact= urlContact
                        print("URL FOUND:"+urlContact.get("href"))
            if finalUrlContact :
                finalUrlContact = finalUrlContact.get("href")
                if not(re.search("http",finalUrlContact,re.IGNORECASE)):
                    finalUrlContact= linkUrl(urlTorequest ,finalUrlContact)
                print("FINAL URL: "+finalUrlContact)
                contactPage = requests.get(finalUrlContact)
                soup = BeautifulSoup(contactPage.text, 'html.parser')
                formFound = soup.find('form')
                if formFound :
                    print("there is form")
                    identifiedForm = []
                    count = 0
                    phoneRequiredString = "Phone not required"
                    for stringToTest in stringToTests:
                        identifiedForm.append(formFound.find_all(string=stringToTest))
                    for whichIdentifiedForm in identifiedForm :
                        if  whichIdentifiedForm:
                            count = count + 1
                    for stringToTest in phoneRequired :
                        if formFound.find_all(string=stringToTest):
                            phoneRequiredString = "Phone required"
                    print("form score is :" + str(count) )
                    fileResult.write(str(lineCount)+","+urlTorequest+","+finalUrlContact+ ",score is:"+str(count)+","+phoneRequiredString)
                else :
                    print("there is no form")
                    fileResult.write(str(lineCount)+", no form")
                fileResult.write("\n")
            else :
                fileResult.write(str(lineCount)+","+urlTorequest+", no contact page \n")

def linkUrl(url1,url2):
    if url2.startswith("/"):
        url2 = url2[1:]
    if url1.endswith("/") :
        url1 = url1[:-1]
    return url1+"/"+url2


if __name__ == "__main__":
    #I do not use regexp because the final user will surely not know that
    stringToTests = ["Email","email","mail","Mail","Name","name","subject","Subject"]
    phoneRequired = ["Phone required","phone required"]
    contactStringCheck = ["Contact","contact","Contacts","contact","Contact us","contact us","Contact Us","CONTACT US"]

    # in case an old file existed, let's erase it to be safe
    csvFile = "biancaBEBE.csv"
    entryCsvFile = "bianca.csv"
    tenPoints(entryCsvFile,csvFile,stringToTests,phoneRequired,contactStringCheck)
    pass

